---
Archive: Bioconductor 3.19
Bug-Database: https://github.com/jipingw/DegNorm/issues
Bug-Submit: https://github.com/jipingw/DegNorm/issues/new
Reference:
 - Author: Bin Xiong and Yiben Yang and Frank R. Fineis and Ji-Ping Wang
   Title: >
    DegNorm: normalization of generalized transcript degradation improves
    accuracy in RNA-seq analysis
   Year: 2019
   Journal: Genome Biology
   Volume: 20
   Pages: 75
   URL: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6466807/
   DOI: 10.1186/s13059-019-1682-7
   ePrint: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6466807/pdf/\
    13059_2019_Article_1682.pdf"
   PMID: 30992037
Registry:
 - Name: conda:bioconda
   Entry: bioconductor-degnorm
 - Name: bio.tools
   Entry: degnorm
Repository: https://github.com/jipingw/DegNorm.git
Repository-Browse: https://github.com/jipingw/DegNorm
